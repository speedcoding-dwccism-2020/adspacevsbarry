var images = [
    'https://images.lanouvellerepublique.fr/image/upload/t_1020w/f_auto/5b95be27be7744fb5c8b467b.jpg',
    'https://upload.wikimedia.org/wikipedia/commons/1/14/Un_super_paysage.jpeg',
    'https://www.competencephoto.com/photo/art/grande/31056991-29406133.jpg?v=1553260189'
]

var i = 0;


function changeImage() {

    $('#col-droite img').attr('src', images[i]);
    i++;
    if (i > images.length) {
        i = 0;
    }

}

function addImage() {
    var newImage = $('#newImage').val();

    images.push(newImage);
    $('#newImage').val('');

}

setInterval(function(){ 
    console.log('array', images);
    changeImage();
}, 1500);